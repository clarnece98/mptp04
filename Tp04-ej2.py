import os
def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')
def menu():
    print('1) Cargar Productos.')
    print('2) Mostrar Productos cargados.')
    print('3) Productos con el stock en un rango')
    print('4) Modificar stock y agregar mas..')
    print('5) Eliminar productos. ')
    print('6) Salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 7)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion

def cPrecio():
    Precio = float(input('Ingrese el precio del producto: '))
    while not Precio >= 0:
        Precio = float(input('Ingrese el precio del producto: '))
    return Precio

def pStock():
    Stock = int(input('ingrese cantidad en stock: '))
    while not Stock >=0:
        Stock = int (input('ingrese cantidad en stock: '))
    return Stock

def leerProducto():
    print('Cargar Lista de Productos')
    Productos = {}
    codigo = -1
    while (codigo != 0):
        codigo = int(input('Codigo (cero para finalizar): '))
        if codigo != 0: 
            if codigo not in Productos:    
                nombre = str(input('Ingrese el nombre del producto: '))
                Precio = cPrecio()
                Stock = pStock()
                if nombre and Precio or Stock:
                    Productos[codigo] = [nombre,Precio,Stock]
                    print('Producto Agregado ')
                    
                elif not nombre or Precio or Stock:
                    print (' ERROR Ingrese todos los datos')
            else:
                print('el producto ya existe')
    os.system('cls')
    return Productos

def mostrar(diccionario):
    print('Listado de Productos')
    for codigo, nombre in diccionario.items():
        print(codigo,nombre)

def rangodestock(diccionario):
    print('Lista de productos en el rango elegido: ')
    desde = int(input('desde : '))
    hasta = int(input('hasta: '))
    for codigo,stock in diccionario.items():
        if (stock[2] >= desde) and (stock[2] <= hasta):
            print(codigo,stock)

def agregarStock(diccionario):
    stockagregar=int(input("rango para agregar stock.."))
    print('Productos con stock menores.. ')
    sumaStock=int(input("Ingrese cantidad a agregar de stock..") )   
    for codigo,stock in diccionario.items():
        if (stockagregar> stock[2]):
            stock[2]+=sumaStock
    print("Los productos fueron agregados con exito....")
    return Productos

def eliminar0(diccionario):
    print('Eliminar los preodcutos con stock 0')
    i=1
    while i <= len(Productos):
        for Codigo,Stock in diccionario.items():
            if (Stock[2]==0):
                print('Se va a elimar: ', Stock[2])
                del diccionario[Codigo]
                print('Procucto eliminado')
                break
        i+=1
    return(diccionario)          

opcion = 0

os.system('cls')
while (opcion != 7):
    opcion = menu()
    if opcion == 1:
        Productos = leerProducto()
        continuar()
    elif opcion == 2:
        mostrar(Productos)
        continuar()
    elif opcion == 3:
        rangodestock(Productos)
        continuar()
    elif opcion==4:
        agregarStock(Productos) 
        continuar()       
    elif opcion == 5:
        eliminar0(Productos)
        continuar()
    
    elif opcion ==6: 
        print('terminado') 
